var express = require('express');
const nodeMailer = require('nodemailer');
var router = express.Router();
const stripe = require("stripe")("sk_test_4dD3iQG5OXQpNpzSJky84OdN");
const Message = require('../models/message');
const About = require('../models/about');

const transporter = nodeMailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
        user: 'todorovmaximm@gmail.com',
        pass: 'pass'
    }
});

const mailOptions = {
    from: '"Candle" <todorovmaximm@gmail.com>', // sender address
    to: 'some@gmail.com', // list of receivers
    subject: 'Candle ID', // Subject line
    text: 'Your id: ', // plain text body
    html: '<b>Your id: </b>' // html body
};

router.get('/', (req, res) => {

    Message.find(function (err, allMessage) {
        if (err) {
            return console.error(err)
        }
        res.send(allMessage)
    }).sort({_id: -1});
});

router.post('/', function (req, res) {
    let msg = new Message(req.body)

    var stream = Message.find().stream();
    var max = 1;

    Message.find().count(function (err, count) {
        if (count <= 0) {
            msg._id = 1;

            msg.save(function (err, creteMsg) {
                if (err) {
                    console.log('err', err)
                }
                console.log('saved Message 1', msg);

                mailOptions.to = msg.email;
                mailOptions.html = "<div><b> Your id: " + msg._id + "</b> <br> <b>Share with your friends:  " +
                    "<a href='http://www.todorov3d.info/webgl/candles?id=" + msg._id + "'>http://www.todorov3d.info/webgl/candles?id=" + msg._id + "</a></b>" +
                    "<br><b>Link for delete: <a href='http://www.todorov3d.info/webgl/candles?remove_id=" + msg._id + "'>http://www.todorov3d.info/webgl/candles?remove_id=" + msg._id + "</a></b></div>";

                transporter.sendMail(mailOptions, (error, info) => {
                    if (error) {
                        return console.log(error);
                    }
                    console.log('Message %s sent: %s', info.messageId, info.response);
                    res.render('index');
                });
            })
            res.sendStatus(200)
        } else {
            stream.on('data', function (doc) {

                var current = parseInt(doc._id, 10);
                if (max < current) {
                    max = current;
                }
            });
            stream.on('close', function () {
                msg._id = (Number(max) + 1)

                msg.save(function (err, creteMsg) {
                    if (err) {
                        console.log('err', err)
                    }
                    console.log('saved Message 2', msg);

                    mailOptions.to = msg.email;
                    mailOptions.html = "<div><b> Your id: " + msg._id + "</b> <br> <b>Share with your friends:  " +
                        "<a href='http://www.todorov3d.info/webgl/candles?id=" + msg._id + "'>http://www.todorov3d.info/webgl/candles?id=" + msg._id + "</a></b>" +
                        "<br><b>Link for delete: <a href='http://www.todorov3d.info/webgl/candles?remove_id=" + msg._id + "'>http://www.todorov3d.info/webgl/candles?remove_id=" + msg._id + "</a></b></div>";

                    transporter.sendMail(mailOptions, (error, info) => {
                        if (error) {
                            return console.log(error);
                        }
                        console.log('Message %s sent: %s', info.messageId, info.response);
                        res.render('index');
                    });
                })
                res.sendStatus(200)
            });
        }
    });

})

router.put('/:id', function (req, res) {

    Message.findOneAndUpdate({"_id": req.params.id}, {"message": req.body.message}, function (err, doc) {
        if (err) {
            console.log('err', err)
        }
        console.log(req.params.id)
    })


    res.sendStatus(200)
})

router.delete('/:id', function (req, res) {
    Message.findOneAndDelete({"_id": req.params.id}, function (err, msgDelete) {
        if (err) {
            console.log('err', err)
        }
        console.log(req.params.id)
    })
    res.sendStatus(200)
});

//----------------------------------------------------------  about

router.post('/about', function (req, res) {
    About.findOneAndUpdate({"_id": 1}, {"about": req.body.about}, function (err, doc) {
        if (err) {
            console.log('err', err)
        }
        if (!doc) {
            let txt = new About({
                _id: 1,
                about: req.body.about
            })

            txt.save(function (err, creteAbout) {
                if (err) return console.error(err);
            })
        }
    })
    res.sendStatus(200)
});

router.get('/about', function (req, res) {
    About.findOne({_id: '1'}, function (err, aboutText) {
        if (err) {
            return console.error(err)
        }
        res.send(aboutText)
    });
});

// ----------------------------------------------------------  payment

router.post('/pay', (req, res) => {
    const token = req.body.stripeToken; // Using Express
    const chargeAmount = req.body.chargeAmount;

    const charge = stripe.charges.create({
        amount: chargeAmount,
        currency: 'usd',
        description: 'Example charge',
        source: token,
    }, (err, charge) => {
        if (err && err.type === "StripeCardError") {
            res.sendStatus(500);
            console.log('Your card was decliened');
        }else{
            res.sendStatus(200);
        }

    });
});


module.exports = router;