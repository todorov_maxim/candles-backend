var mongoose = require('mongoose');

var aboutSchema = new mongoose.Schema({
    _id: Number,
    about: [String]
}, {_id: false});

var About = mongoose.model('About', aboutSchema);

module.exports = About;