var mongoose = require('mongoose');

var msgSchema = new mongoose.Schema({
    _id: Number,
    message: String,
    payment: Number,
    email: String,
    name: String,
    coord: {
        x: Number,
        y: Number,
        z: Number
    }
}, {_id: false});

var Message = mongoose.model('Message', msgSchema);

module.exports = Message;