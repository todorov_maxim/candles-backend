const express = require('express');
const cors = require('cors');
const path = require('path');
var apiRouter = require('./routers/api')
var authRouter = require('./routers/auth')


const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use('/api', apiRouter);
app.use('/auth', authRouter);

app.use(express.static(__dirname + '/front/'));

app.get('*', function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
    res.header("Access-Control-Allow-Headers", "X-Requested-With,content-type, Authorization");

    res.contentType("text/html; charset=utf-8");

    res.sendFile(path.join(__dirname + '/front/index.html'));
});


module.exports = app;

