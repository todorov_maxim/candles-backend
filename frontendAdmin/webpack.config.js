const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const argv = require('yargs').argv;
const isDevelopment = argv.mode === 'development';
const isProduction = !isDevelopment;
const distPath = path.join(__dirname, '/dist');

const config = { 
    entry: {
        app: './src/index.js'
      },
      output: {
        filename: './js/bundle.js',
        path: distPath
      },
      module: {
        rules: [
            {
                test: /\.html$/,
                exclude: /node_modules/,
                use: 'html-loader'
            }, 
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [{
                    loader: 'babel-loader'
                }]
            },
            {
              test: /\.scss$/,
              exclude: /node_modules/,
              use: [
                isDevelopment ? 'style-loader' : MiniCssExtractPlugin.loader,
                'css-loader',
                {
                  loader: 'postcss-loader',
                  options: {
                    plugins: [
                      isProduction ? require('cssnano') : () => {},
                      require('autoprefixer')({
                        browsers: ['last 2 versions']
                      })
                    ]
                  }
                },
                {
                  loader: 'sass-loader',
                  query: {
                      sourceMap: false,
                  }
                }
              ]
            }
        ]
      },
      plugins: [
        new MiniCssExtractPlugin({
          filename: 'css/[name].css',
          chunkFilename: '[id].css'
        }),
        new HtmlWebpackPlugin({
          template: './public/index.html',
        })
      ],
      optimization: isProduction ? {
        minimizer: [
          new UglifyJsPlugin({
            sourceMap: true,
            uglifyOptions: {
              compress: {
                inline: false,
                drop_console: true
              },
            },
          }),
        ],
      } : {},
      devServer: {
        contentBase: distPath,
        port: 9000,
        compress: true,
        open: true,
        historyApiFallback: true
      }
}

module.exports = (env) =>{
  config.devtool = isProduction ? 'source-map' : 'eval-sourcemap';
  return config;
};