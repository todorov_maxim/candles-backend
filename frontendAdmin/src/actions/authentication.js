import axios from 'axios';
import {url} from './../config';

export const loginUser = (data) => dispatch => {
    axios.post(url + '/auth', data).then(res => {
        if (res.status == 200) {
            localStorage.setItem('auth', "root");
            dispatch({type: 'ADD_USER', user: "root"});
        }
        /* localStorage.setItem('auth', "root");
         dispatch({ type: 'ADD_USER', user:  "root"});*/
    })
        .catch(err => {
            console.log(err.response.data)
        })

}

export const logoutUser = () => dispatch => {
    localStorage.removeItem('auth');
    dispatch({type: 'ADD_USER', user: {}});
}