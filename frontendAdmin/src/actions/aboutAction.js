import axios from 'axios';
import {url} from './../config';

export const getAbout = () => dispatch => {
    axios.get(url + '/api/about')
        .then(res => {
            let arr = res.data.about;
            var str = arr.join('\n\n');
            dispatch({type: 'SHOWABOUT', data: str});
        })

}
export const aboutAction = (str) => dispatch => {
    dispatch({type: 'SHOWABOUT', data: str});
}

export const updAbout = function (textAbout){
    axios.post(url + '/api/about', textAbout)

}