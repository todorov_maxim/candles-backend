import axios from 'axios';
import {url} from './../config';


export const viewerAction = () => dispatch => {
    axios.get(url + '/api')
        .then(res => {
            dispatch({type: 'SHOW', data: res.data});

        })

}
export const deleteAction = (item) => dispatch => {
    axios.delete(url + '/api/' + item)
        .then(res => {
            dispatch({type: 'DEL', data: item});

        })

}

export const addAction = (item) => dispatch => {
    axios.post(url + '/api', item)
        .then(res => {
            dispatch({type: 'ADD', data: item});

        })

}

export const updAction = (msg, item) => dispatch => {
    axios.put(url + '/api/' + item, msg)
        .then(res => {
            msg.id = item;
            dispatch({type: 'UPD', data: msg});

        })

}