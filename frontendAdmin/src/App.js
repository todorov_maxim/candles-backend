import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import store from './store';
import { Provider } from 'react-redux';

import ViewerComponent from './components/ViewerComponent';
import AddComponent from './components/AddComponent';
import LoginComponent from './components/LoginComponent';
import HeaderComponent from './components/HeaderComponent';
import EditAboutComponent from './components/EditAboutComponent'

if (localStorage.auth){
    store.dispatch({ type: 'ADD_USER', user: "root" });
  }

class App extends Component{
    render(){
        return(
            <Provider store = { store }>
                <Router>
                    <div>
                        <Route exact path="/auth"  component={ LoginComponent } /> 
                        <Route exact path="/" component={ HeaderComponent } />
                        <Route exact path="/" component={ EditAboutComponent } />
                        <Route exact path="/" component={ AddComponent } />
                        <Route exact path="/" component={ ViewerComponent } />
                        
                        
                    </div>
                </Router>
            </Provider>
        )
    }
}

export default App;