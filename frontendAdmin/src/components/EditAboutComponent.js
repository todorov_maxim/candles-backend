import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getAbout, aboutAction, updAbout  } from '../actions/aboutAction';

class EditAboutComponent extends Component{
    constructor(props) {
        super(props);
        
        this.aboutChange = this.aboutChange.bind(this)
        this.aboutSubmit = this.aboutSubmit.bind(this)
      }
    aboutSubmit(event){
        event.preventDefault();
        let str = this.props.about.textAbout;
        var arr = str.split('\n\n');
        let test = {
            "about" : arr
        }
        if (str.length > 350){
            alert("Limit the number of characters entered. \nMaximum amount: 350 \nYou entered:" + str.length)
        }else{
            updAbout(test)
            let a = document.getElementById('text').classList
            a.add("text-info")
            setTimeout(function(){ a.remove("text-info") }, 3000);
            
        }
    }
    componentWillMount(){
        this.props.getAbout()
        let arr = this.props.about.textAbout;
        var str = arr.join('\n\n');
        this.setState({ about : str});  
        
    }
    aboutChange(event){
        this.props.aboutAction(event.target.value)
        
    }
    render(){
        return(
            <div className="add">
                <label className="label">About Us</label>
                <form onSubmit={this.aboutSubmit} className='form'>
                        <div className="textarea-about">
                            <textarea type='text' name='about' value={this.props.about.textAbout} onChange={this.aboutChange} />
                        </div>
                        <div className="btn-wrapper">
                            <div id="text">Info updated</div>
                            <button className="btn">update info</button> 
                        </div>
                    </form>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    about: state.about
  })

export default connect(mapStateToProps, { getAbout, aboutAction })(EditAboutComponent);