import React, { Component } from 'react';
import { connect } from 'react-redux';
import { loginUser } from '../actions/authentication';

class LoginComponent extends Component {
  constructor(){
    super();
    this.state = {
      username: '',
      password: ''
    }
    this.loginChange = this.loginChange.bind(this);
    this.loginSubmit= this.loginSubmit.bind(this);
  }

  loginChange(event){
    this.setState({[event.target.name] : event.target.value});
  }
  loginSubmit(event){
    event.preventDefault();
    let data = {
      "login" : this.state.username,
      "pass" : this.state.password
    }
    this.props.loginUser(data)
  }

  componentDidMount() {
    if(this.props.auth.isAuthenticated) {
        this.props.history.push('/');
    }
}
  componentWillReceiveProps(nextProps) {
    if(nextProps.auth.isAuthenticated) {
        this.props.history.push('/')
        
    }
  }

  render() {
    return (
      <div className="main-wrapper">
        <div className="form-container">
          <h2 className="h2">welcome back</h2>
          <div className="form-wrapper">
            <form onSubmit={this.loginSubmit} className='auth-form'>
              <div className="input-wrapper">
                  <label>login</label>
                  <input type='text' name='username' value={this.state.username} onChange={this.loginChange} />
              </div>
              <div className="input-wrapper">
                  <label>Password</label>
                  <input type='password' name='password' value={this.state.password} onChange={this.loginChange} />
              </div>
              <div className="btn-wrapper">
                <button className="btn">Сome in</button> 
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}


const mapStateToProps = (state) => ({
  auth: state.auth
})
export  default connect(mapStateToProps, { loginUser })(LoginComponent)