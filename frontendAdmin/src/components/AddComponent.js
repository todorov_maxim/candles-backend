import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addAction } from '../actions/viewerActions';

class AddComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message : '',
            payment : '',
            x : '',
            y : '',
            z : '',
            name: '',
            email: ''
        };
        this.dataSubmit = this.dataSubmit.bind(this);
        this.messageChange = this.messageChange.bind(this);
      }
      dataSubmit(event){
        event.preventDefault();
        let data = {
        "coord": {
            "x": Number(this.state.x),
            "y": Number(this.state.y),
            "z": Number(this.state.z)
        },
        "message" : this.state.message,
        "payment" : Number(this.state.payment),
        "name": this.state.name,
        "email": this.state.email
        }
        this.props.addAction(data)
        this.setState({
            message : '',
            payment : '',
            x : '',
            y : '',
            z : '',
            name: '',
            email: ''
        })
      }
      
      messageChange(event){
        this.setState({[event.target.name] : event.target.value});
      }
    render() {
        
        return (
            <div className="add">
                <label className='label'>Register a new item</label>
              <div className="form-wrapper formadd">
                    <form onSubmit={this.dataSubmit} className='form'>
                        <div className="textarea-wrapper">
                            <label>Message</label>
                            <textarea type='text' name='message' value={this.state.message} onChange={this.messageChange} />
                        </div>
                        <div className="input-containner">
                            <div className="input-wrapper">
                                <label>name</label>
                                <input type='text' name='name' value={this.state.name} onChange={this.messageChange} />
                            </div>
                            <div className="input-wrapper">
                                <label>email</label>
                                <input type='text' name='email' value={this.state.email} onChange={this.messageChange} />
                            </div>
                            <div className="input-wrapper">
                                <label>Payment</label>
                                <input type='text' name='payment' value={this.state.payment} onChange={this.messageChange} />
                            </div>
                            <div className="input-wrapper">
                                <label>Value X</label>
                                <input type='text' name='x' value={this.state.x} onChange={this.messageChange} />
                            </div>
                            <div className="input-wrapper">
                                <label>Value Y</label>
                                <input type='text' name='y' value={this.state.y} onChange={this.messageChange} />
                            </div>
                            <div className="input-wrapper">
                                <label>Value Z</label>
                                <input type='text' name='z' value={this.state.z} onChange={this.messageChange} />
                            </div>
                        </div>
                        
                        <div className="btn-wrapper">
                            <button className="btn">Add</button> 
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    view: state.view
  })

export default connect(mapStateToProps, { addAction })(AddComponent);
