import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { logoutUser } from '../actions/authentication';



class NavbarComponent extends Component{

    onlogoutUser(event){
        event.preventDefault();
        this.props.logoutUser();
        this.props.history.push('/auth');
    }
    
    render(){
        const {isAuthenticated, user} = this.props.auth;
        const authLinks = (
            <ul>
                <li><Link to="/auth" className="log-btn">Login</Link></li>
            </ul>
        )
        const guestLinks = (
            <div className="user-info">
                <div>
                    <div className="name">Hello {user}</div>
                </div>
                <div className="out">
                    <a href="/" onClick={this.onlogoutUser.bind(this)}>Exit</a>
                </div>
            </div>
        )
        return(
            <nav>
                <div className="container nav-container">
                    {isAuthenticated ? guestLinks : authLinks}
                </div>
            </nav>
        )
    }
}

const mapStateToProps = (state) => ({
    auth: state.auth
})

export default connect(
    mapStateToProps,
      { logoutUser }
)(withRouter(NavbarComponent));