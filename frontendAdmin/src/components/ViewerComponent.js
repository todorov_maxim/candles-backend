import React, { Component } from 'react';
import { connect } from 'react-redux';
import { viewerAction, deleteAction, updAction } from '../actions/viewerActions';

class ViewerComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            message: ''
        };
        this.messageChange = this.messageChange.bind(this);
        this.dataSubmit = this.dataSubmit.bind(this);

      }
    componentDidMount(){
        if(!this.props.auth.isAuthenticated){
            this.props.history.push('/auth');
        }
    }
    componentWillMount(){
        this.props.viewerAction()
    }
    deleteLine(item){
        this.props.deleteAction(item)
    }
    updateLine(event){
        this.setState({ id : event});
        document.getElementById("modal").classList.remove("hide");
    }
    messageChange(event){
        this.setState({[event.target.name] : event.target.value});
    }
    dataSubmit(event){
        event.preventDefault();
        let data = {
            "message" : this.state.message
            
        }
        this.props.updAction(data, this.state.id)
        document.getElementById("modal").classList.add("hide");
    }
    modalClose(){
        document.getElementById("modal").classList.add("hide");
    }
    
    render() {
        const entry = this.props.view.viewer
        return (
            <div className="viewer add">
                 <label className='label'>Show list</label>
                    <div  className="table">
                    <div className="line">
                    <div className="column">id</div>
                    <div className="column">name</div>
                    <div className="column">email</div>
                    <div className="column">message</div>
                    <div className="column">payment</div>
                    <div className="column">coord</div>
                    <div className="column"></div>
                    <div className="column"></div>
                    </div>
                    </div>
              {entry.map(item => (
                        <div key={entry.indexOf(item)} className="table">
                            <div className="line">
                                <div className="column">{item._id}</div>
                                <div className="column">{item.name}</div>
                                <div className="column">{item.email}</div>
                                <div className="column">{item.message}</div>
                                <div className="column">{item.payment}</div>
                                <div className="column">X:{item.coord.x} Y:{item.coord.y} Z:{item.coord.z}</div>
                                <div className="column btn-wrapper btn-upd">
                                    <button onClick={this.updateLine.bind(this, item._id)} className="btn">Update</button> 
                                </div>
                                <div  className="column btn-wrapper btn-del">
                                    <button onClick={this.deleteLine.bind(this, item._id)} className="btn">Delete</button> 
                                </div>
                            </div>
                        </div>
                ))}
                <div  className="modal-conntainer hide" id="modal">
                    <div className="form-wrapper modal-wrap" >
                        <button onClick={this.modalClose.bind(this)}  className="btn-close">Сlose</button> 
                        <form onSubmit={this.dataSubmit} className='form'>
                            <div className="textarea-mod">
                                <label>Update message</label>
                                <textarea type='texterra' name='message' value={this.state.message} onChange={this.messageChange} />
                            </div>
                            <div className="btn-wrapper">
                                <button className="btn">Update</button> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    view: state.view,
    auth: state.auth
  })

export default connect(mapStateToProps, { viewerAction, deleteAction, updAction })(ViewerComponent);