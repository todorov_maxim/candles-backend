const initialState = {
    textAbout : []
}

function aboutReducer( state = initialState, action){
    if (action.type === 'SHOWABOUT'){
        return {
            textAbout: action.data
        }
    }

    return state;
}

export default aboutReducer;