
const initialState = {

    viewer : []
}

function mainReduser( state = initialState, action){
    if (action.type === 'SHOW'){
        return {
            viewer: action.data
        }
    }else if (action.type === 'DEL'){
        return {
             viewer: state.viewer.filter( eventItem => eventItem._id !== action.data )
        }
    }else if (action.type === 'ADD'){
        var max = 1;
        state.viewer.forEach((item)=>{
            if (max < item._id) {
                max = item._id;
            } 
        })
        action.data._id = (Number(max) + 1)
        return {
             viewer: [action.data, ...state.viewer]
        }
    }else if(action.type === 'UPD'){
        state.viewer.forEach((item)=>{
            if (item._id == action.data.id) {
                item.message = action.data.message
            } 
        })
        return {
            viewer: state.viewer
       }
    }
    return state;
}

export default mainReduser;