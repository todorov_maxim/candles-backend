import { combineReducers } from 'redux';
import viewerReduser from './viewerReduser';
import authReducer from './authReduser';
import aboutReducer from './aboutReduser'

export default combineReducers({
    auth: authReducer,
    view: viewerReduser,
    about: aboutReducer
})