import { isEmpty } from '../validation/isEmpty';
const initialState = {
    isAuthenticated: false,
    user: {}
}

function profile(state=initialState, action){
    if (action.type === 'ADD_USER'){
        return {
            isAuthenticated: !isEmpty(action.user),
            user: action.user
        }
    }
    return state;
  }

  export default profile;